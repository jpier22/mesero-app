import { Injectable } from '@angular/core';
import { Tienda } from './../_model/tienda';
import { HttpClient, HttpHeaders } from '@angular/common/http';
import { Subject } from 'rxjs/Subject';


@Injectable({
  providedIn: 'root'
})
export class TiendaService {

  url: string = "http://localhost:3000/tienda";
  tiendaCambio = new Subject<Tienda>();

  constructor(private http: HttpClient) { }

  getTienda(id: string) {
    let access_token = JSON.parse(sessionStorage.getItem("access_token")).token;
    return this.http.get<Tienda>(`${this.url}/leer/${id}`, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    });
  }

  actualizarTienda(tienda: Tienda) {
    let access_token = JSON.parse(sessionStorage.getItem("access_token")).token;
    return this.http.put(`${this.url}/actualizar`, tienda, {
      headers: new HttpHeaders().set('Authorization', `bearer ${access_token}`).set('Content-Type', 'application/json')
    }).subscribe(data => {
      if (data === 1) {
        this.getTienda(tienda._id.toString()).subscribe(tienda => {
          this.tiendaCambio.next(tienda);
        });
      }
    });
  }
}
