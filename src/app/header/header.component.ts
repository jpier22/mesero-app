import { LoginService } from './../_service/login.service';
import { Component, OnInit } from '@angular/core';
import { Router, ActivatedRoute } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';

@Component({
  selector: 'app-header',
  templateUrl: './header.component.html',
  styleUrls: ['./header.component.css']
})
export class HeaderComponent implements OnInit {

  idTienda: string;
  constructor(private loginService: LoginService, private router: Router,
    private route: ActivatedRoute) { }

  ngOnInit() {

    const helper = new JwtHelperService();
    let tk = JSON.parse(sessionStorage.getItem('access_token'));
    const decodedToken = helper.decodeToken(tk.token);
    this.idTienda = decodedToken.user.usuario.idTienda;

  }

  cerrarSesion() {
    this.loginService.cerrarSesion();
  }
}
