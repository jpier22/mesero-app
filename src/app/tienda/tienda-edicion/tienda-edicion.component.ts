import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { TiendaService } from 'src/app/_service/tienda.service';
import { FormGroup, FormControl } from '@angular/forms';
import { Tienda } from 'src/app/_model/tienda';


@Component({
  selector: 'app-tienda-edicion',
  templateUrl: './tienda-edicion.component.html',
  styleUrls: ['./tienda-edicion.component.css']
})
export class TiendaEdicionComponent implements OnInit {

  id: string;
  form: FormGroup;

  constructor(private tiendaService: TiendaService, private route: ActivatedRoute, private router: Router) {
    this.form = new FormGroup({
      'id': new FormControl(''),
      'nombre': new FormControl(''),
      'direccion': new FormControl(''),
      'fotoUrl': new FormControl('')
    });
  }

  ngOnInit() {
    this.route.params.subscribe((params: Params) => {
      this.id = params['id'];
      this.initForm();
    });
  }

  operar() {
    let tienda = new Tienda(this.form.value['id'], this.form.value['nombre'], this.form.value['direccion'], this.form.value['fotoUrl']);

    this.tiendaService.actualizarTienda(tienda);

    this.router.navigate(['../../'], { relativeTo: this.route });
  }

  initForm() {

    this.tiendaService.getTienda(this.id).subscribe(data => {

      let id = 0;
      let nombre = '';
      let direccion = '';
      let fotoUrl = '';

      id = data._id;
      nombre = data.nombre;
      direccion = data.direccion;
      fotoUrl = data.fotoUrl;

      this.form = new FormGroup({
        'id': new FormControl(id),
        'nombre': new FormControl(nombre),
        'direccion': new FormControl(direccion),
        'fotoUrl': new FormControl(fotoUrl)
      });
    });
  }

}