import { Component, OnInit } from '@angular/core';

import { TiendaService } from './../../_service/tienda.service';
import { ActivatedRoute, Params, Router } from '@angular/router';
import { JwtHelperService } from '@auth0/angular-jwt';
import { Tienda } from '../../_model/tienda';

@Component({
  selector: 'app-tienda-detalle',
  templateUrl: './tienda-detalle.component.html',
  styleUrls: ['./tienda-detalle.component.css']
})
export class TiendaDetalleComponent implements OnInit {

  tienda: Tienda;
  id: string;

  constructor(private tiendaService: TiendaService, private route: ActivatedRoute, private router: Router) { }

  ngOnInit() {

    const helper = new JwtHelperService();

    this.tiendaService.tiendaCambio.subscribe(data => {
      this.tienda = data;
    });

    let tk = JSON.parse(sessionStorage.getItem('access_token'));
    const decodedToken = helper.decodeToken(tk.token);
    this.id = decodedToken.user.usuario.idTienda;

    this.tiendaService.getTienda(this.id).subscribe(data => {
      this.tienda = data;
    });

  }

  editarTienda() {
    this.router.navigate([this.id, 'editar'], { relativeTo: this.route });
  }

}
