export class Tienda {
    _id: number;
    nombre: string;
    direccion: string;
    fotoUrl: string;

    constructor(id: number, nombre: string, direccion: string, fotoUrl: string) {
        this._id = id;
        this.nombre = nombre;
        this.direccion = direccion;
        this.fotoUrl = fotoUrl;
    }
}